import { App } from 'vue';

export default (app: App) => {
    app.directive('dragSize', (el, binding) => {
        // 拖拽区域
        const dragArea = el.querySelector(binding.value.dragArea);
        // 改变大小区域
        const resizeArea = el.querySelector(binding.value.resizeArea);
        // 触发拖拽部分的宽度
        const dragAreaWidth = binding.value.dragAreaWidth;

        // 获取当前浏览器窗口宽高（高度减去任务栏）
        const clientWidth = document.documentElement.clientWidth;
        const clientHeight = document.documentElement.clientHeight - 40;

        dragArea.onmousemove = (e: MouseEvent) => {
            // 鼠标落点标志
            let pointFlag = '';

            // 鼠标位置
            const mouseX = e.clientX;
            const mouseY = e.clientY;
            // 获取元素宽度和高度
            const resizeAreaWidth = el.offsetWidth;
            const resizeAreaHeight = el.offsetHeight;
            // 元素相对偏移量
            const dragAreaX = dragArea.getBoundingClientRect().left;
            const dragAreaY = dragArea.getBoundingClientRect().top;
            // 鼠标相对位置
            const mouseOfDragAreaX = mouseX - dragAreaX;
            const mouseOfDragAreaY = mouseY - dragAreaY;

            // 判断鼠标落点
            if (mouseOfDragAreaX <= dragAreaWidth && mouseOfDragAreaY <= dragAreaWidth) {
                // 左上
                dragArea.style.cursor = 'nwse-resize';
                pointFlag = 'lt';
            } else if (mouseOfDragAreaX <= dragAreaWidth && mouseOfDragAreaY >= resizeAreaHeight) {
                // 左下
                dragArea.style.cursor = 'nesw-resize';
                pointFlag = 'lb';
            } else if (mouseOfDragAreaX >= resizeAreaWidth && mouseOfDragAreaY <= dragAreaWidth) {
                // 右上
                dragArea.style.cursor = 'nesw-resize';
                pointFlag = 'rt';
            } else if (mouseOfDragAreaX >= resizeAreaWidth && mouseOfDragAreaY >= resizeAreaHeight) {
                // 右下
                dragArea.style.cursor = 'nwse-resize';
                pointFlag = 'rb';
            } else if (mouseOfDragAreaX <= dragAreaWidth) {
                // 左边
                dragArea.style.cursor = 'ew-resize';
                pointFlag = 'l';
            } else if (mouseOfDragAreaY <= dragAreaWidth) {
                // 上边
                dragArea.style.cursor = 'ns-resize';
                pointFlag = 't';
            } else if (mouseOfDragAreaX >= resizeAreaWidth) {
                // 右边
                dragArea.style.cursor = 'ew-resize';
                pointFlag = 'r';
            } else if (mouseOfDragAreaY >= resizeAreaHeight) {
                // 下边
                dragArea.style.cursor = 'ns-resize';
                pointFlag = 'b';
            } else {
                // 其他
                dragArea.style.cursor = 'Default';
                pointFlag = '';
            }

            // 按下鼠标处理事件
            dragArea.onmousedown = (e: MouseEvent) => {
                // 按下时落点标志
                const currentPointFlag = pointFlag;
                // 按下时鼠标位置
                const mouseX = e.clientX;
                const mouseY = e.clientY;
                // 按下时元素宽高
                const resizeAreaWidth = resizeArea.offsetWidth;
                const resizeAreaHeight = resizeArea.offsetHeight;
                // 按下时元素相对偏移量
                const elX = el.offsetLeft;
                const elY = el.offsetTop;

                // 鼠标移动事件
                document.onmousemove = (e) => {
                    let currentMouseX = e.clientX;
                    let currentMouseY = e.clientY;
                    // 限制定位不能移出浏览器（预留10px的空间）
                    if (currentMouseX <= 10) {
                        currentMouseX = 10;
                    } else if (currentMouseX >= clientWidth - 10) {
                        currentMouseX = clientWidth;
                    }
                    if (currentMouseY <= 10) {
                        currentMouseY = 10;
                    } else if (currentMouseY >= clientHeight - 10) {
                        currentMouseY = clientHeight;
                    }
                    // 计算需要偏移的距离
                    const left = currentMouseX - mouseX;
                    const top = currentMouseY - mouseY;

                    // 根据鼠标落点计算宽高
                    if (currentPointFlag === 'lt') {
                        resizeArea.style.width = resizeAreaWidth - left + 'px';
                        resizeArea.style.height = resizeAreaHeight - top + 'px';
                        el.style.left = elX + left + 'px';
                        el.style.top = elY + top + 'px';
                    } else if (currentPointFlag === 'lb') {
                        resizeArea.style.width = resizeAreaWidth - left + 'px';
                        resizeArea.style.height = resizeAreaHeight + top + 'px';
                        el.style.left = elX + left + 'px';
                    } else if (currentPointFlag === 'rt') {
                        resizeArea.style.width = resizeAreaWidth + left + 'px';
                        resizeArea.style.height = resizeAreaHeight - top + 'px';
                        el.style.top = elY + top + 'px';
                    } else if (currentPointFlag === 'rb') {
                        resizeArea.style.width = resizeAreaWidth + left + 'px';
                        resizeArea.style.height = resizeAreaHeight + top + 'px';
                    } else if (currentPointFlag === 'l') {
                        resizeArea.style.width = resizeAreaWidth - left + 'px';
                        el.style.left = elX + left + 'px';
                    } else if (currentPointFlag === 't') {
                        resizeArea.style.height = resizeAreaHeight - top + 'px';
                        el.style.top = elY + top + 'px';
                    } else if (currentPointFlag === 'r') {
                        resizeArea.style.width = resizeAreaWidth + left + 'px';
                    } else if (currentPointFlag === 'b') {
                        resizeArea.style.height = resizeAreaHeight + top + 'px';
                    }
                };

                // 鼠标松开事件
                document.onmouseup = () => {
                    document.onmousemove = null;
                    document.onmouseup = null;
                };
            };
        };
    });
};
