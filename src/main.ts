import { createApp } from 'vue';
import App from './App.vue';

import './assets/styles/index.less';

// vue-router4
import Router from './router';

// Pinia
import Store from './store';

// 窗口拖拽指令
import DragMove from './directives/drag-move';
// 窗口调整大小指令
import DragSize from './directives/drag-size';

// iconfont
import './assets/iconfont/iconfont.css';

createApp(App).use(Router).use(Store).use(DragMove).use(DragSize).mount('#app');
