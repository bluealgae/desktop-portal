import { createRouter, createWebHashHistory } from 'vue-router';
import { useUserStore } from '@/store/user';

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('@/views/Home.vue'),
        meta: { title: '主页' },
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/Login.vue'),
        meta: { title: '登录', isPublic: true },
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    // 创建store对象
    const userStore = useUserStore();
    if (to.meta.isPublic || userStore.isLogged) {
        next();
    } else {
        next('/login');
    }
});

export default router;
