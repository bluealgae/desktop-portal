import { defineStore } from 'pinia';

export const useShortcutStore = defineStore({
    id: 'shortcut',
    state: () => {
        return {
            // 存放快捷方式对象
            shortcutArray: new Array<{ id: string; icon: string; name: string; url: string }>(),
        };
    },
    actions: {
        // 追加一个快捷方式对象
        push(shortcut: { id: string; icon: string; name: string; url: string }) {
            this.shortcutArray.push(shortcut);
        },
        // 删除一个快捷方式对象
        remove(shortcutId: string) {
            const findIndex = this.shortcutArray.findIndex((shortcut) => shortcut.id === shortcutId);
            this.shortcutArray.splice(findIndex, 1);
        },
    },
});
