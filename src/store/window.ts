import { defineStore } from 'pinia';

export const useWindowStore = defineStore({
    id: 'window',
    state: () => {
        return {
            // 存放窗口对象
            windowArray: new Array<{
                id: string;
                icon: string;
                title: string;
                url: string;
                isMin?: boolean;
            }>(),
            // 存放窗口id，该数组的索引可作为z-index样式值
            zIndexArray: new Array<string>(),
        };
    },
    actions: {
        // 追加一个窗口对象
        push(window: { id: string; icon: string; title: string; url: string; isMin?: boolean }) {
            this.windowArray.push(window);
            this.zIndexArray.push(window.id);
        },
        // 移除一个窗口对象
        remove(windowId: string) {
            const findIndex1 = this.windowArray.findIndex((window) => window.id === windowId);
            this.windowArray.splice(findIndex1, 1);
            const findIndex2 = this.zIndexArray.findIndex((id) => id === windowId);
            this.zIndexArray.splice(findIndex2, 1);
        },
        // 获取zIndex值
        getZIndex(windowId: string) {
            return this.zIndexArray.findIndex((id) => id === windowId) + 100;
        },
        // 将该窗口id移动到最后
        goTop(windowId: string) {
            // 少于2个窗口无需操作
            if (this.zIndexArray.length < 2) {
                return;
            }
            // 找到当前位置
            const findIndex = this.zIndexArray.findIndex((id) => id === windowId);
            // 已经置顶无需操作
            if (findIndex === this.zIndexArray.length - 1) {
                return;
            }
            // 是否找到
            if (findIndex > -1) {
                // 后移
                for (let i = findIndex; i < this.zIndexArray.length - 1; i++) {
                    this.zIndexArray[i] = this.zIndexArray[i + 1];
                }
                // 设置到最后
                this.zIndexArray[this.zIndexArray.length - 1] = windowId;
            }
        },
        // 是否已经置顶
        isTop(windowId: string) {
            // 找到当前位置
            const findIndex = this.zIndexArray.findIndex((id) => id === windowId);
            // 已经置顶无需操作
            return findIndex === this.zIndexArray.length - 1;
        },
        // 将该窗口最小化
        min(windowId: string) {
            // 找到当前位置
            const window = this.windowArray.find((window) => window.id === windowId);
            // 是否找到
            if (window) {
                window.isMin = true;

                // zIndexArray中移动至首位
                if (this.zIndexArray.length > 1) {
                    for (let i = this.zIndexArray.length - 1; i > 0; i--) {
                        this.zIndexArray[i] = this.zIndexArray[i - 1];
                    }
                    this.zIndexArray[0] = windowId;
                }
            }
        },
        // 将该窗口取消最小化
        cancelMin(windowId: string) {
            // 找到当前位置
            const window = this.windowArray.find((window) => window.id === windowId);
            // 是否找到
            if (window) {
                window.isMin = false;
            }
        },
        // 是否已经最小化
        isMin(windowId: string) {
            // 找到当前位置
            const window = this.windowArray.find((window) => window.id === windowId);
            // 是否找到
            if (window) {
                return window.isMin;
            } else {
                return false;
            }
        },
    },
});
