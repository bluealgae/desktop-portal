# 仿Windows桌面门户系统

![项目演示图](image.png)

> 作者：Junki

## 一、简介

> 核心技术框架：Vite2 + Vue3 + TypeScript

> 预览地址：[http://101.37.18.4:9999](http://101.37.18.4:9999) 用户名和密码随便填

> 该项目正在初期开发状态，欢迎star和建议

## 二、写给小白

### 1.相关命令

> 安装依赖：```yarn```

> 开发环境运行：```yarn dev```

> 构建生产包：```yarn build```

### 2.eslint和prettier配置

#### WebStorm：

- 进入 ```File | Settings | Languages & Frameworks | JavaScript | Code Quality Tools | ESLint```，
  开启自动配置，勾选```Run Eslint --fix on save```
- 进入 ```File | Settings | Languages & Frameworks | JavaScript | Prettier```，勾选```On 'Reformat Code' action```
  ，在```Run for files```中添加vue文件类型
